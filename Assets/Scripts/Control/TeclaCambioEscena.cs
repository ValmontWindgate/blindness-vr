﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeclaCambioEscena : MonoBehaviour {

	public int tiempo;

	// Update is called once per frame
	void Update () {
		tiempo++;
		if (Input.GetKeyDown(KeyCode.Alpha1)) SceneManager.LoadScene(0);
		if (Input.GetKeyDown(KeyCode.Alpha2)) SceneManager.LoadScene(1);

	}
}
