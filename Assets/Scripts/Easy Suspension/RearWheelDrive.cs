﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RearWheelDrive : MonoBehaviour {

	private WheelCollider[] wheels;
	public Rigidbody rigidbody;
	public Text speedText;

	public float maxAngle = 30;
	public float maxTorque = 300;
	public GameObject wheelShape;
	public float posSteerWheels = 4f;

	// here we find all the WheelColliders down in the hierarchy
	public void Start()
	{
		wheels = GetComponentsInChildren<WheelCollider>();

		for (int i = 0; i < wheels.Length; ++i) 
		{
			var wheel = wheels [i];

			// create wheel shapes only when needed
			if (wheelShape != null)
			{
				var ws = GameObject.Instantiate (wheelShape);
				ws.transform.parent = wheel.transform;
			}
		}
	}

	// this is a really simple approach to updating wheels
	// here we simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero
	// this helps us to figure our which wheels are front ones and which are rear
	public void Update()
	{

		speedText.text = "SPEED: " + rigidbody.velocity.magnitude*3.6f + " km/h";

		float angle = maxAngle * Input.GetAxis("Horizontal");
		float torque = maxTorque * Input.GetAxis("Vertical");
		// float torque = maxTorque;

		foreach (WheelCollider wheel in wheels) {
			// a simple car where front wheels steer while rear ones drive
			if (wheel.transform.localPosition.z > posSteerWheels) {
				wheel.steerAngle = angle;
				if (torque < 0)
					wheel.brakeTorque = -torque*10;
				else
					wheel.brakeTorque = 0;
			}

			if (wheel.transform.localPosition.z < posSteerWheels){
				if (torque > 0) {
					wheel.motorTorque = torque;
					wheel.brakeTorque = 0;
				}
				else
					wheel.brakeTorque = -torque*10;
			}

			// update visual wheels if any
			if (wheelShape) 
			{
				Quaternion q;
				Vector3 p;
				wheel.GetWorldPose (out p, out q);

				// assume that the only child of the wheelcollider is the wheel shape
				Transform shapeTransform = wheel.transform.GetChild (0);
				shapeTransform.position = p;
				shapeTransform.rotation = q;
			}

		}
	}
}
