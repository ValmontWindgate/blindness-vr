﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

public class PuertaVR : MonoBehaviour {

	// Variables de acceso al Animator principal de la puerta y al script de
	//  interacción VR
	public Animator animator;	
	public VRInteractiveItem vrInteractiveItemScript;
	public GameObject canvas;
	public bool abierta;	// Para obtener y asignar el estado abierta/cerrada

	// Al iniciar apilamos el evento de abrir como respuesta al clic
	void Start() {
		vrInteractiveItemScript.OnClick += Abrir;
		vrInteractiveItemScript.OnClick += CambiarLayer;
		vrInteractiveItemScript.OnOver += ActivarCanvas;
		vrInteractiveItemScript.OnOut += DesactivarCanvas;
	}

	// Invierte el estado del booleano y lo mete a la máquina de estados
	public void Abrir ( ) {
		abierta = animator.GetBool ("Abrir");
		abierta = !abierta;
		animator.SetBool("Abrir", abierta);
	}

	public void CambiarLayer ( ) {
		gameObject.layer = 14;
	}

	public void ActivarCanvas ( ) {
		canvas.SetActive(true);
	}
	public void DesactivarCanvas() {
		canvas.SetActive(false);
	}

}
