﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSRigidbodyVRPC : MonoBehaviour {

	public GameObject pivoteGiroX, pivoteGiroY;		// Nos permitirán girar en PC con el ratón!
	public GameObject camaraVR;	// Todo controlador VR requiere acceso a la cámara
	public Rigidbody rigidbody;
	public float velocidad = 2f;
	public float giro = 360f;
	public float impulsoSalto = 10f;

	// Update is called once per frame
	void Update ( ) {
		
		// Giro en la versión PC
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_WEBGL
			pivoteGiroX.transform.Rotate ( Vector3.up * giro * Time.deltaTime * Input.GetAxis ( "Mouse X") , Space.World );
			pivoteGiroY.transform.Rotate ( -Vector3.right * giro * Time.deltaTime * Input.GetAxis ( "Mouse Y"));
		#endif

		if ( Input.GetButtonDown ( "Jump" ) ) {
			rigidbody.AddForce ( Vector3.up * impulsoSalto * rigidbody.mass , ForceMode.Impulse );
		}

		// Obtenemos la forward de la cámara, avanzaremos hacia donde mire
		Vector3 camaraForward = camaraVR.transform.forward;

		// Proyectamos la forward en el suelo, para que no suba/baje
		camaraForward.y = 0;

		// Memorizamos su velocidad de caída en Y
		float velocidadY = rigidbody.velocity.y;		
		// Asignamos la velocidad hacia donde miramos (cameraVR.forward)
		rigidbody.velocity = Input.GetAxis ( "Vertical" ) * velocidad * camaraForward;
		// RESTAURAMOS la velocidad de caída en Y para que pueda caer libremente
		rigidbody.velocity = new Vector3 ( rigidbody.velocity.x , velocidadY , rigidbody.velocity.z );

	}

}
