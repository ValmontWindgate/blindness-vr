﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorFPSVR : MonoBehaviour {

	public GameObject cameraVR;     // Acceso a la cámara VR para saber a dónde miramos
	public GameObject pivoteX, pivoteY;		// Para poder girar con el mouse en modo PC
	public Rigidbody rigidbody;
	public float velocidad = 2f;
	public float giro = 360f;
	public float salto = 10f;
	public Vector3 avance;
	
	// Update is called once per frame
	void Update ( ) {

		// Controles de giro de la cámara en la versión PC
		#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
		pivoteX.transform.Rotate(Vector3.up * giro * Time.deltaTime * Input.GetAxis("Mouse X"));
		pivoteY.transform.Rotate(-Vector3.right * giro * Time.deltaTime * Input.GetAxis("Mouse Y"));
		#endif

		// Control del salto con la tecla Espacio
		if ( Input.GetButtonDown ( "Jump") ) {
			rigidbody.AddForce(Vector3.up * salto, ForceMode.Impulse);
		}

		// Obtenemos la forward de la cámara y anulamos su Y
		avance = cameraVR.transform.forward;
		avance.y = 0;

		// Velocidad de avance en la dirección a la que mira la cámara
		avance = avance * Input.GetAxis("Vertical") * velocidad;

		// Reasignamos la velocidad del rigidbody para que vaya pacia donde nosotros queremos
		float velocidadY = rigidbody.velocity.y;	// Memorizamos la velocidad de caída
		rigidbody.velocity = avance;                // Sobreescribimos el vector de velocidad
		rigidbody.velocity = new Vector3 ( rigidbody.velocity.x , velocidadY , rigidbody.velocity.z );

	}

}
