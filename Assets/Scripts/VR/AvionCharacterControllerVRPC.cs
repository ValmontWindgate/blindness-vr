﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvionCharacterControllerVRPC : MonoBehaviour {

	public GameObject pivoteGiroX, pivoteGiroY;		// Nos permitirán girar en PC con el ratón!
	public GameObject camaraVR;	// Todo controlador VR requiere acceso a la cámara
	public GameObject modelo;
	public CharacterController characterController;
	public float velocidad = 2f;
	public float giro = 360f;	// Sólo para la versión PC (No hay gafas)

	// Update is called once per frame
	void Update ( ) {
		
		// Giro en la versión PC
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_WEBGL
			pivoteGiroX.transform.Rotate ( Vector3.up * giro * Time.deltaTime * Input.GetAxis ( "Mouse X") , Space.World );
			pivoteGiroY.transform.Rotate ( -Vector3.right * giro * Time.deltaTime * Input.GetAxis ( "Mouse Y"));
		#endif

		// Asignamos al modelo la misma rotación que la cámara
		modelo.transform.rotation = camaraVR.transform.rotation;

		// Obtenemos la forward de la cámara, avanzaremos hacia donde mire
		Vector3 camaraForward = camaraVR.transform.forward;

		// Movimiento final del controlador hacia donde mira la cámara
		characterController.Move ( camaraForward * velocidad * Time.deltaTime );

	}

}
