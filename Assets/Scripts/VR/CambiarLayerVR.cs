﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

public class CambiarLayerVR : MonoBehaviour {

	public VRInteractiveItem vrInteractiveItemScript;
	public bool visible;

	void OnEnable() {
		vrInteractiveItemScript = GetComponent<VRInteractiveItem>();
		vrInteractiveItemScript.OnClick += CambiarLayer;
	}
	void OnDisable() {
		vrInteractiveItemScript.OnClick += CambiarLayer;
	}

	public void CambiarLayer ( ) {
		gameObject.layer = 14;
	}

}
