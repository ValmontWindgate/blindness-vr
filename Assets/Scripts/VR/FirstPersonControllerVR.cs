﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonControllerVR : MonoBehaviour {

	public GameObject pivoteGiroX, pivoteGiroY;		// Nos permitirán girar en PC con el ratón!
	public GameObject camaraVR;	// Todo controlador VR requiere acceso a la cámara
	public CharacterController characterController;
	public float velocidad = 2f;
	public float giro = 360f;

	// Update is called once per frame
	void Update ( ) {
		
		// Giro en la versión PC
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_WEBGL
			pivoteGiroX.transform.Rotate ( Vector3.up * giro * Time.deltaTime * Input.GetAxis ( "Mouse X") , Space.World );
			pivoteGiroY.transform.Rotate ( -Vector3.right * giro * Time.deltaTime * Input.GetAxis ( "Mouse Y"));
		#endif

		// Obtenemos la forward de la cámara, avanzaremos hacia donde mire
		Vector3 camaraForward = camaraVR.transform.forward;

		// Proyectamos la forward en el suelo, para que no suba/baje
		camaraForward.y = 0;

		// camaraForward.Normalize ( );		// Podemos normalizarlo para que mida 1...

		// Finalmente movemos hacia donde miramos, a la velocidad que hemos puesto por segundo	
		characterController.Move ( Input.GetAxis ( "Vertical" ) * velocidad * Time.deltaTime *
			camaraForward );

	}

}
