﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLerp : MonoBehaviour {

	public GameObject pivote;
	public float suavidadPosicion = 0.1f;
	public float suavidadGiro = 0.1f;
	
	// Update is called once per frame
	void FixedUpdate () {

		transform.position = Vector3.Lerp ( transform.position ,		// Acercamos posición
			pivote.transform.position , suavidadPosicion );				//  suavemente

		transform.rotation = Quaternion.Lerp ( transform.rotation ,		// Acercamos rotación
			pivote.transform.rotation , suavidadGiro );					//  suavemente

	}
}
