﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridVR : MonoBehaviour {

	public GameObject personaje;
	public MeshRenderer meshRenderer;
	
	void Update () {
		transform.position = personaje.transform.position;
		transform.Translate(Vector3.down);
		meshRenderer.material.mainTextureOffset = new Vector2(
			(-transform.position.x) % 1,
			(-transform.position.z) % 1
		);
	}
}
